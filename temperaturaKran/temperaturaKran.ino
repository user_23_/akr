#include <Temperature_LM75_Derived.h>
Generic_LM75 temperature;

void setup() {
 Serial.begin(9600);
 Wire.begin();
}

void loop() {
  Serial.print("Temperature =");
  Serial.print(temperature.readTemperatureC());     
  Serial.println(" C");
  delay(250);
}
