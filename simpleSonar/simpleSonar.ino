#include <LiquidCrystal_I2C.h> // Библиотека I2C дисплея
LiquidCrystal_I2C lcd(0x27, 16, 2);

void setup() {
  Serial.begin(9600);
  int pot = A7;
  lcd.init(); // Инициализация LCD
  lcd.begin(16, 1);  // Задаем размерность экрана
}

void loop() {
  // Проверка потенциометра
  int pot = A7;
  int val = analogRead(pot);
  // Проверка экрана 1602
  //lcd.clear(); // Очищаем экран перед получением нового значения
  lcd.setCursor(0, 0); // курсор на 4-й символ 1-й строки
  lcd.print("  val  "); // Тест на 1-й строке экрана
  lcd.setCursor(2, 1); // курсор на 7-й символ 2-й строки
  lcd.print(val); // Значение t на 2-й строке экрана
}
