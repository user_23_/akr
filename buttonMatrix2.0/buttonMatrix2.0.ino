#include <LedControl.h>
LedControl lc = LedControl(5, 6, 7, 1);
  const int BUTTON_13 = 13;
  const int BUTTON_16 = 16;
  const int BUTTON_17 = 17;




void setup() {
  lc.shutdown(0, false);
  lc.setIntensity(0, 1);
  lc.clearDisplay(0);

}

void loop() {
  bool button1 = digitalRead (13);
  bool button2 = digitalRead (16);
  bool button3 = digitalRead (17);
  if ( button1 == true) {
   smaleOn1();
  }
  else {
   smaleOff();
  }
  if ( button2 == true) {
   smaleOn2();
  }
  else {
   smaleOff();
  }
  if ( button3 == true) {
   smaleOn3();
  }
  else {
   smaleOff();
  }
}

void smaleOn1() {
  lc.setRow(0, 1, B11100111);
  lc.setRow(0, 2, B11000011);
  lc.setRow(0, 3, B10000001);
  lc.setRow(0, 4, B10000001);
  lc.setRow(0, 5, B10000001);
  lc.setRow(0, 6, B10000001);
  lc.setRow(0, 7, B11000011);
  lc.setRow(0, 8, B11100111);
} 
void smaleOn2() {
  lc.setRow(0, 1, B00011000);
  lc.setRow(0, 2, B00111100);
  lc.setRow(0, 3, B01111110);
  lc.setRow(0, 4, B11111111);
  lc.setRow(0, 5, B11111111);
  lc.setRow(0, 6, B01111110);
  lc.setRow(0, 7, B00111100);
  lc.setRow(0, 8, B00011000);
}
void smaleOn3() {
  lc.setRow(0, 1, B11111111);
  lc.setRow(0, 2, B11111111);
  lc.setRow(0, 3, B11111111);
  lc.setRow(0, 4, B11111111);
  lc.setRow(0, 5, B11111111);
  lc.setRow(0, 6, B11111111);
  lc.setRow(0, 7, B11111111);
  lc.setRow(0, 8, B11111111);
}
void smaleOff() {
  lc.setRow(0, 1, B00000000);
  lc.setRow(0, 2, B00000000);
  lc.setRow(0, 3, B00000000);
  lc.setRow(0, 4, B00000000);
  lc.setRow(0, 5, B00000000);
  lc.setRow(0, 6, B00000000);
  lc.setRow(0, 7, B00000000);
  lc.setRow(0, 8, B00000000);
}
